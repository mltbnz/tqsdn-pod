# ThreeQSDN

[![CI Status](http://img.shields.io/travis/Malte Fentroß/ThreeQSDN.svg?style=flat)](https://travis-ci.org/Malte Fentroß/ThreeQSDN)
[![Version](https://img.shields.io/cocoapods/v/ThreeQSDN.svg?style=flat)](http://cocoapods.org/pods/ThreeQSDN)
[![License](https://img.shields.io/cocoapods/l/ThreeQSDN.svg?style=flat)](http://cocoapods.org/pods/ThreeQSDN)
[![Platform](https://img.shields.io/cocoapods/p/ThreeQSDN.svg?style=flat)](http://cocoapods.org/pods/ThreeQSDN)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ThreeQSDN is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ThreeQSDN"
```

## Author

Malte Fentroß, m.fentross@app-buddy.de

## License

ThreeQSDN is available under the MIT license. See the LICENSE file for more info.
