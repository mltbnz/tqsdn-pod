/**
 * ViewTools.h
 *
 * \brief     Short description.
 * \details   Detailed description.
 * \author Kirill (c)
 * \date 10.05.14
 */

#import "ViewTools.h"


@implementation ViewTools {
    
}

+ (void)embedChildView:(UIView *)child intoParent:(UIView *)parent
{
    [parent addSubview:child];
    parent.clipsToBounds = YES;
    
    [child setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSLayoutConstraint *top = [NSLayoutConstraint
                               constraintWithItem:child
                               attribute:NSLayoutAttributeTop
                               relatedBy:NSLayoutRelationEqual
                               toItem:parent
                               attribute:NSLayoutAttributeTop
                               multiplier:1.0f
                               constant:0.f];
    NSLayoutConstraint *bottom =[NSLayoutConstraint
                                 constraintWithItem:child
                                 attribute:NSLayoutAttributeBottom
                                 relatedBy:0
                                 toItem:parent
                                 attribute:NSLayoutAttributeBottom
                                 multiplier:1.0
                                 constant:0];
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   constraintWithItem:child
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:parent
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0f
                                   constant:0.f];
    NSLayoutConstraint *trailing =[NSLayoutConstraint
                                   constraintWithItem:child
                                   attribute:NSLayoutAttributeTrailing
                                   relatedBy:0
                                   toItem:parent
                                   attribute:NSLayoutAttributeTrailing
                                   multiplier:1.0
                                   constant:0];
    
    [parent addConstraint:top];
    [parent addConstraint:bottom];
    [parent addConstraint:leading];
    [parent addConstraint:trailing];
}

@end
