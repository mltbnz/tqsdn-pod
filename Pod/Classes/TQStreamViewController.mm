//
//  TQStreamViewController.m
//  ThreeQSDN
//
//  Created by Malte Fentroß on 02.07.15.
//  Copyright (c) 2015 Malte Fentroß. All rights reserved.
//

#import "TQStreamViewController.h"
#import "TQStreamManager.h"
#import "ViewTools.h"

@interface TQStreamViewController ()

@property (assign, nonatomic) NSInteger streamID;

// is properly connected to stream
@property (assign, nonatomic) BOOL connected;

// is streaming data to server
@property (assign, nonatomic) BOOL streaming;

// our rtmp session
@property (strong, nonatomic) VCSimpleSession *streamSession;

// the endpoint to connect to
@property (strong, nonatomic) NSString *publishingPoint;

// the internal stream name
@property (strong, nonatomic) NSString *streamName;

// optional values
// bitrate of the video
@property (assign, nonatomic) int videoBitrate;

// height of the video
@property (assign, nonatomic) CGFloat videoHeight;

// width of the video
@property (assign, nonatomic) CGFloat videoWidth;

// initial value of the idle timer, to be restored
@property (assign, nonatomic) BOOL idleTimerDisabled;

@end

@implementation TQStreamViewController

// the current connection status to save and load
// it is readonly from other classes
@synthesize connectionStatus = _connectionStatus;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.idleTimerDisabled = [UIApplication sharedApplication].idleTimerDisabled;
    
    if(self.streamID > 0) {
        [self _connect];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [UIApplication sharedApplication].idleTimerDisabled = YES;
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [UIApplication sharedApplication].idleTimerDisabled = self.idleTimerDisabled;
}

// start streaming content of camera
- (void) startStream {
    
    NSAssert(self.publishingPoint != nil && self.streamName != nil, @"Stream credentials not given!");
    
    [self.streamSession startRtmpSessionWithURL:@"rtmp://de-origin-live.3qsdn.com/live/" andStreamKey:self.streamName];
    self.streaming = YES;
    
}


// stop streaming. preview view will stay
- (void) stopStream {
    NSLog(@"EDNING RTMP SESSION!!!!!!");
    [self.streamSession endRtmpSession];
    self.streaming = NO;
}

// switch camera
- (void) toggleCamera {
    
    if(self.streamSession.cameraState == VCCameraStateBack)
        [self.streamSession setCameraState:VCCameraStateFront];
    else
        [self.streamSession setCameraState:VCCameraStateBack];
    
}

// toggle turn on / off flash light
- (void) toggleFlashlight {
    [self.streamSession setTorch:!self.streamSession.torch];
}

// focus on given point
// (0,0) is top-left, (1,1) is bottom-right
- (void) setFocusOnPoint:(CGPoint)point {
    
    NSAssert(point.x >= 0.0 && point.x <= 1.0 && point.y >= 0.0 && point.y <= 1.0,
           @"Focus points must be  0 <= x <= 1");
    
    [self.streamSession setFocusPointOfInterest:point];
}

// set zoom factor
- (void) setZoomFactor:(CGFloat)zoom {
    [self.streamSession setVideoZoomFactor:zoom];
}

#pragma mark - private necessary functions
- (void) _connect {
    NSAssert(self.streamID > 0, @"No stream id was given!");
    
    [TQStreamManager getPublishingPointForStream:self.streamID onCompletion:^(NSString *publishingPoint, NSString *streamName, NSError *error) {
        
        // set height if given
        CGFloat height      = (self.videoHeight)    ? self.videoHeight : self.view.bounds.size.height;
        
        // set width if given
        CGFloat width       = (self.videoWidth)     ? self.videoWidth : self.view.bounds.size.width;
        
        // set height if given
        int bitrate  = (self.videoBitrate)   ? self.videoBitrate : 4000000;
        
        self.streamSession = [[VCSimpleSession alloc] initWithVideoSize:CGSizeMake(width, height)
                                                              frameRate:30
                                                                bitrate:bitrate
                                                useInterfaceOrientation:NO
                                                        cameraState:VCCameraStateBack];
        self.streamSession.delegate = self;
        
//        [ViewTools embedChildView:self.streamSession.previewView intoParent:self.view];
        self.streamSession.previewView.frame = self.view.frame;
        [self.view addSubview:self.streamSession.previewView];
        self.streamSession.orientationLocked = NO;
        
        self.publishingPoint = publishingPoint;
        self.streamName = streamName;
        
    }];
    
}

// function to call delegate
- (void) notifyDelegateOfConnectionStatusChange {
    if ([self.delegate respondsToSelector:@selector(updatedStreamStatus:forStream:)]) {
        [self.delegate updatedStreamStatus:self.connectionStatus forStream:self.streamID];
    }
}

#pragma mark - Video Core delegates
- (void) connectionStatusChanged:(VCSessionState) state {
    switch(state) {
        case VCSessionStateStarting:
            _connectionStatus = kTQConnecting;
            break;
        case VCSessionStateStarted:
            _connectionStatus = kTQConnected;
            break;
        default:
            _connectionStatus = kTQDisconnected;
            break;
    }
    [self notifyDelegateOfConnectionStatusChange];
}

#pragma mark - Initializing functions

- (id) initWithStreamID:(NSInteger)streamID {
    self = [super init];
    if(self) {
        self.streamID = streamID;
    }
    return self;
}

- (id) initWithStreamID:(NSInteger)streamID
             withHeight:(CGFloat)height
               andWidth:(CGFloat)width
        andVideoBitrate:(int)bitrate {
    self = [super init];
    if(self) {
        self.streamID = streamID;
        self.videoBitrate = bitrate;
        self.videoHeight = height;
        self.videoWidth = width;
    }
    return self;
}

@end
