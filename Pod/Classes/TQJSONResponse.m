//
//  TQJSONResponse.m
//  ThreeQSDN
//
//  Created by Malte Fentroß on 02.07.15.
//  Copyright © 2015 Malte Fentroß. All rights reserved.
//

#import "TQJSONResponse.h"

@implementation TQJSONResponse

+(BOOL)propertyIsOptional:(NSString*)propertyName {
    
    if([propertyName isEqualToString:@"Code"]) {
        return NO;
    }
    
    return YES;
}

@end
