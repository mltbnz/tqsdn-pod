//
//  TQStreamManager.m
//  ThreeQSDN
//
//  Created by Malte Fentroß on 02.07.15.
//  Copyright © 2015 Malte Fentroß. All rights reserved.
//

#import "TQStreamManager.h"
#import "TQJSONResponse.h"
#import "JSONModel+networking.h"

@implementation TQStreamManager

#pragma mark - Private access variables

static NSString *TQ_ACCESS_KEY = @"_not_set_";
static NSString *TQ_MAIN_URL = @"_not_set_";

#pragma mark - Public functions

+(void) initializeWithKey:(NSString *)key {
    TQ_ACCESS_KEY   = key;
    TQ_MAIN_URL     = [NSString stringWithFormat:@"https://sdn.3qsdn.com/de/api/?accesskey=%@", TQ_ACCESS_KEY];
    // TODO: do additionally stuff ... save key in storage?
}

+(void) createStreamWithLabel:(NSString *)label
                 onCompletion:(void(^)(NSInteger streamID, NSError *error)) block {
    [self _preRequest];
    
//    NSString *parsedLabel   = [label stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *parsedLabel   = [label stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSString *url           = [NSString stringWithFormat:@"%@&action=create_stream&streamtype=3&label=%@",
                               TQ_MAIN_URL, parsedLabel];
    
    [JSONHTTPClient getJSONFromURLWithString:url completion:^(id json, JSONModelError *err) {
        
        NSError *jsonError = nil;
        TQJSONResponse *response = [[TQJSONResponse alloc] initWithDictionary:json error:&jsonError];
        
        if(jsonError) {
            // result data was wrong
            block(0, jsonError);
        
        } else if(response.Code != 200) {
            
            NSLog(@"Error while creating stream: %@", response.Message);
            
            NSError *apiError = [self _errorObjectWithCode:response.Code andMessage:response.Message];
            
            block(0, apiError);
            
        } else {
            // everything went fine
            block(response.StreamId, nil);
        }
        
    }];
    
}

+(void) createStreamWithLabel:(NSString *)label
                 withMetaData:(NSDictionary *)metaData
                   autorecord:(BOOL)autorecord
                 onCompletion:(void(^)(NSInteger streamID, NSError *error)) block {
    
    [self _preRequest];
    
    NSString *paramString = @"";
    if(metaData) {
        for (NSString *key in metaData) {
            NSString *value = [metaData objectForKey:key];
            NSString *param = [NSString stringWithFormat:@"&%@=%@", key, value];
            paramString = [paramString stringByAppendingString:param];
        }
    }
    
    NSString *autorecordParam = (autorecord) ? @"true" : @"false";
    
    NSString *parsedLabel   = [label stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *url           = [NSString stringWithFormat:@"%@&action=create_stream&streamtype=3&label=%@&autorecord=%@%@",
                               TQ_MAIN_URL, parsedLabel, autorecordParam, paramString];
    
    [JSONHTTPClient getJSONFromURLWithString:url completion:^(id json, JSONModelError *err) {
        
        NSError *jsonError = nil;
        TQJSONResponse *response = [[TQJSONResponse alloc] initWithDictionary:json error:&jsonError];
        
        if(jsonError) {
            // result data was wrong
            block(0, jsonError);
            
        } else if(response.Code != 200) {
            
            NSLog(@"Error while creating stream: %@", response.Message);
            
            NSError *apiError = [self _errorObjectWithCode:response.Code andMessage:response.Message];
            
            block(0, apiError);
            
        } else {
            // everything went fine
            block(response.StreamId, nil);
        }
        
    }];

    
}

+(void) deleteStreamWithID:(NSInteger)streamID
              onCompletion:(void(^)(NSError *error)) block {
    [self _preRequest];
    
    NSString *url = [NSString stringWithFormat:@"%@&action=delete_stream&id=%zd",
                     TQ_MAIN_URL, streamID];
    
    [JSONHTTPClient getJSONFromURLWithString:url completion:^(id json, JSONModelError *err) {
        
        NSError *jsonError = nil;
        TQJSONResponse *response = [[TQJSONResponse alloc] initWithDictionary:json error:&jsonError];
        
        if(jsonError) {
            // result data was wrong
            block(jsonError);
            
        } else if(response.Code != 200) {
            
            NSLog(@"Error while deleting stream: %@", response.Message);
            block(err);
            
        } else {
            // everything went fine
            block(nil);
        }
        
    }];
    
}

+(void) getPublishingPointForStream:(NSInteger)streamID
                       onCompletion:(void(^)(NSString *publishingPoint,
                                             NSString *streamName,
                                             NSError *error))block {
    [self _preRequest];
    
    NSString *url = [NSString stringWithFormat:@"%@&action=get_ingest&id=%zd", TQ_MAIN_URL, streamID];
    
    [JSONHTTPClient getJSONFromURLWithString:url completion:^(id json, JSONModelError *err) {
        
        NSError *jsonError  = nil;
        
        TQJSONResponse *response = [[TQJSONResponse alloc] initWithDictionary:json[0] error:&jsonError];
        
        if(jsonError) {
            // result data was wrong
            block(nil, nil, jsonError);
            
        } else if(response.Code != 200) {
            
            NSLog(@"Error while retrieving publishing point: %@", response.Message);
            block(nil, nil, err);
            
        } else {
            // everything went fine
            block(response.PublishingPoint, response.StreamName, nil);
        }
        
    }];
    
}

// FIXME: this is not working by now because url seems to be wrong
+(void) getStatusOfStream:(NSInteger)streamID
             onCompletion:(void(^)(BOOL isOnline,
                                   NSInteger listeners,
                                   NSError *error))block {
    
    [self _preRequest];
    
    NSString *url = [NSString stringWithFormat:@"%@&action=get_streamstatus&id=%zd", TQ_MAIN_URL, streamID];
    
    [JSONHTTPClient getJSONFromURLWithString:url completion:^(id json, JSONModelError *err) {
        
        NSError *jsonError  = nil;
        
        TQJSONResponse *response = [[TQJSONResponse alloc] initWithDictionary:json[0] error:&jsonError];
        
        if(jsonError) {
            // result data was wrong
            block(NO, 0, jsonError);
            
        } else if(response.Code != 200) {
            
            NSLog(@"Error while retrieving status of stream: %@", response.Message);
            block(NO, 0, err);
            
        } else {
            // everything went fine
            block(response.IsOnline, response.Listeners, nil);
        }
        
    }];

}

+(void) getStreamingURLs:(NSInteger)streamID
            onCompletion:(void(^)(TQJSONURLModel *originalURLs,
                                  NSArray *transcodedURLs,
                                  NSError *error))block {
    
    [self _preRequest];
    
    NSString *url = [NSString stringWithFormat:@"%@&action=get_streamplayout&id=%zd", TQ_MAIN_URL, streamID];
    
    [JSONHTTPClient getJSONFromURLWithString:url completion:^(id json, JSONModelError *err) {
        
        NSError *jsonError  = nil;
        
        TQJSONResponse *response = [[TQJSONResponse alloc] initWithDictionary:json[0] error:&jsonError];
        
        if(jsonError) {
            // result data was wrong
            block(nil, nil, jsonError);
            
        } else if(response.Code != 200) {
            
            NSLog(@"Error while retrieving play out urls of stream: %@", response.Message);
            block(nil, nil, err);
            
        } else {
            // everything went fine
            block(response.SourceOutput, response.TranscoderOutput, nil);
        }
        
    }];
    
}

// set meta data for given stream
// tags is nsarray of nsstring s
+(void) setMetaDataForStream:(NSInteger)streamID
                  parameters:(NSDictionary *)parameters
                        tags:(NSArray *)tags{
    
    [self _preRequest];
    
    NSString *tagString = @"";
    
    // if tags are set
    if(tags) {
        for (int i = 0; i < tags.count; i++) {
            tagString = [tagString stringByAppendingString:tags[i]];
            if(i != tags.count - 1)
                tagString = [tagString stringByAppendingString:@"%20"];
        }
    }
    
    NSString *paramString = @"";
    if(parameters) {
        for (NSString *key in parameters) {
            NSString *value = [parameters objectForKey:key];
            NSString *param = [NSString stringWithFormat:@"&%@=%@", key, value];
            paramString = [paramString stringByAppendingString:param];
        }
    }
    
    NSString *url = [NSString stringWithFormat:@"%@&action=set_streammetadata&stream=%zd&tags=%@%@", TQ_MAIN_URL, streamID, tagString, paramString];
    url = [url stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [JSONHTTPClient getJSONFromURLWithString:url completion:^(id json, JSONModelError *err) {
        
    }];
    
}

#pragma mark - private functions

// create an error object with given code and message
+(NSError*) _errorObjectWithCode:(NSInteger)code andMessage:(NSString *)message {
    NSDictionary *errorDictionary = @{ NSLocalizedDescriptionKey : message };
    
    return [[NSError alloc] initWithDomain:@"de.3qsdn"
                                      code:code
                                  userInfo:errorDictionary];
}

#pragma mark - internal validations

// check if everything is ok and request may be executed
+(void) _preRequest {
    NSAssert(![TQ_ACCESS_KEY isEqualToString:@"_not_set_"],
             @"You have to set an access key for the 3QSDN webservice!");
}

@end