//
//  TQViewController.m
//  ThreeQSDN
//
//  Created by Malte Fentroß on 07/02/2015.
//  Copyright (c) 2014 Malte Fentroß. All rights reserved.
//

#import "TQViewController.h"
#import "TQStreamManager.h"
#import "ViewTools.h"

@interface TQViewController ()

@end

@implementation TQViewController {
    NSInteger _streamID;
    TQStreamViewController *_svc;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [TQStreamManager createStreamWithLabel:@"test stream aus app"
//                              onCompletion:^(NSInteger streamID, NSError *error) {
//        
//                                  if(!error) {
//                                      _streamID = streamID;
////                                      [self testStatus];
//                                      _svc = [[TQStreamViewController alloc] initWithStreamID:streamID];
//
//                                      [ViewTools embedChildView:_svc.view intoParent:self.view];
//                                      [self.view bringSubviewToFront:self.startStreamButton];
//                                      [self.view bringSubviewToFront:self.cameraButton];
//                                      [self.view bringSubviewToFront:self.flashButton];
//                                  }
//                                  
//    }];
    
    _streamID = 3256;
    _svc = [[TQStreamViewController alloc] initWithStreamID:_streamID
                                                 withHeight:1920.0
                                                   andWidth:1080.0
                                            andVideoBitrate:700000];
    _svc.delegate = self;
    
     [ViewTools embedChildView:_svc.view intoParent:self.view];
     [self.view bringSubviewToFront:self.startStreamButton];
     [self.view bringSubviewToFront:self.cameraButton];
     [self.view bringSubviewToFront:self.flashButton];
    
    [self testPlayOut];
    
    [NSTimer scheduledTimerWithTimeInterval:3.0 target:_svc selector:@selector(startStream) userInfo:nil repeats:NO];
    
	// Do any additional setup after loading the view, typically from a nib.
}


//FIXME: make unit tests for following functions

- (void)testPlayOut {
    [TQStreamManager getStreamingURLs:_streamID onCompletion:^(TQJSONURLModel *originalURLs, NSArray *transcodedURLs, NSError *error) {
        NSLog(@"Received stream urls");
//        [_svc startStream];
    }];
}

- (void)testStatus {
    [TQStreamManager getPublishingPointForStream:_streamID
                                    onCompletion:^(NSString *publishingPoint,
                                                   NSString *streamName,
                                                   NSError  *error) {
                                        if(error) {
                                            NSLog(@"Error in retrieving status: %@", error.description);
                                        } else {
                                            NSLog(@"Publishing point: %@", publishingPoint);
                                            NSLog(@"Stream Name: %@", streamName);
                                            [self testStatusTwo];
                                        }
                                        
                                        
    }];
}

- (void) testStatusTwo {
    [TQStreamManager getStatusOfStream:_streamID onCompletion:^(BOOL isOnline, NSInteger listeners, NSError *error) {
       
        if(error) {
            NSLog(@"Error while testing status of stream: %@", error.description);
        } else {
            NSLog(@"Stream is online: %@", (isOnline ? @"Yes" : @"No"));
            NSLog(@"Number of people watching: %zd", listeners);
            [self testDelete];
        }
        
    }];
}

- (void)testDelete {
    [TQStreamManager deleteStreamWithID:_streamID onCompletion:^(NSError *error) {
        if(error) {
            NSLog(@"Error in deleting stream: %@", error.description);
        } else {
            NSLog(@"Successfully deleted stream!");
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)startStreamButtonTouched:(id)sender {
    [_svc startStream];
}

- (IBAction)toggleFlashlight:(id)sender {
    [_svc toggleFlashlight];
}

- (IBAction)toggleCamera:(id)sender {
    [_svc toggleCamera];
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - stream delegates
- (void) updatedStreamStatus:(TQStreamConnectionStatusType)status forStream:(NSInteger)streamID {
    NSLog(@"Changed stream status to: %i", status);
}


@end
